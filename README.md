# (Long) linked lists for R

This is a fun project that creates (long) linked lists out of [data.table](https://cran.r-project.org/package=data.table)s in R.

## Linked lists with `llist`

```r
o <- new_llist("value")
append(o, "next")
store(o, 1, "other")
prepend(o, "first")
trash(o, 1)
```

## Long linked lists with `lllist`

```r
o <- new_lllist("value")
store(o, c(1, 2), "slot 2 in position 1")
append(o, "slot 1 in position 2")
prepend(o, "now at first position")
trash(o, 2)
```
